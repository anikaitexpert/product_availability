import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
declare var RazorpayCheckout: any;

@Component({
  selector: 'app-sentizer',
  templateUrl: './sentizer.component.html',
  styleUrls: ['./sentizer.component.scss'],
})
export class SentizerComponent implements OnInit {
  categories: any;
  submitted = false;
  public registerForm: FormGroup;
  loading = false;
  loading_message = "";
  loading2 = false;

  paymentAmount: number = 0;
  currency: string = 'INR';
  currencyIcon: string = '₹';
  razor_key = 'rzp_live_7yJo5t6IE3wOeB';
  //kwk4GK9Ymu3p5O1rO0ZDrioF
  cardDetails: any = {};

  validate;
  quantity;
  rate;
  discount;
  mrp;
  full_name;
  email;
  phone;
  city;
  state;
  amount;
  delivery_date;
  pay_method: number;

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {
    this.pay_method = 0;

  }
  order() {
    if (this.pay_method == 0) {
      this.payment_online();
    }
    if (this.pay_method == 1) {
      this.payment_offline();
    }

  }

  payment_online() {
    if (this.quantity == null) {
      alert("Please enter quantity");
      this.validate = false;
      return;
    }

    if (this.full_name == null) {
      alert("Your name should be greater than 5 characters");
      this.validate = false;
      return;
    }

    if (this.phone == null) {
      alert("Please enter valid mobile no.");
      this.validate = false;
      return;
    }

    if (this.email == null) {
      alert("Please enter valid email id.");
      this.validate = false;
      return;
    }

    if (this.city == null) {
      alert("Please enter your city");
      this.validate = false;
      return;
    }

    if (this.state == null) {
      alert("Please select state");
      this.validate = false;
      return;
    }

    this.validate = true;
    // let data = 'quantity=' + this.quantity + '&rate=' + this.rate + '&discount=' + this.discount
    //   + '&mrp=' + this.mrp + '&full_name=' + this.full_name + '&email=' + this.email +
    //   '&phone=' + this.phone + '&city=' + this.city + '&state=' + this.state + '&delivery_date=' + this.delivery_date +
    //   '&amount=' + this.amount + '&payment_id=12345';
    // this.post(data);

    this.paymentAmount = (this.amount * 100) / 2;
    var options = {
      description: 'Solace World',
      image: 'https://solacebiotech.in/app/icon2.png',
      currency: this.currency,
      key: this.razor_key,
      amount: this.paymentAmount,
      name: this.full_name,
      prefill: {
        email: this.email,
        contact: this.phone,
        name: this.full_name
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function () {
          alert('dismissed')
        }
      }
    };

    var successCallback = (payment_id) => {
      let data = 'quantity=' + this.quantity + '&rate=' + this.rate + '&discount=' + this.discount
        + '&mrp=' + this.mrp + '&full_name=' + this.full_name + '&email=' + this.email +
        '&phone=' + this.phone + '&city=' + this.city + '&state=' + this.state + '&delivery_date=' + this.delivery_date +
        '&amount=' + this.amount + '&payment_id=' + payment_id+"&status=1";
      this.post(data);

    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

  payment_offline() {
    if (this.quantity == null) {
      alert("Please enter quantity");
      this.validate = false;
      return;
    }

    if (this.full_name == null) {
      alert("Your name should be greater than 5 characters");
      this.validate = false;
      return;
    }

    if (this.phone == null) {
      alert("Please enter valid mobile no.");
      this.validate = false;
      return;
    }

    if (this.email == null) {
      alert("Please enter valid email id.");
      this.validate = false;
      return;
    }

    if (this.city == null) {
      alert("Please enter your city");
      this.validate = false;
      return;
    }

    if (this.state == null) {
      alert("Please select state");
      this.validate = false;
      return;
    }

    this.validate = true;
    let data = 'quantity=' + this.quantity + '&rate=' + this.rate + '&discount=' + this.discount
      + '&mrp=' + this.mrp + '&full_name=' + this.full_name + '&email=' + this.email +
      '&phone=' + this.phone + '&city=' + this.city + '&state=' + this.state + '&delivery_date=' + this.delivery_date +
      '&amount=' + this.amount + '&payment_id=offline=status=0';
    this.post(data);
  }

  post(data) {
    this.apiService.zermeroOrder(data).subscribe((data: any) => {
      if (data.status == 1) {
        this.quantity = null;
        this.rate = null;
        this.discount = null;
        this.mrp = null;
        this.full_name = null;
        this.email = null;
        this.phone = null;
        this.city = null;
        this.state = null;
        this.amount = null;
        this.delivery_date = null;
        this.commonService.presentAlert("Success", "Thank you. Your order is successfully submitted.");
      }
      else {
        alert("Error occured!");
      }

    })
  }
  changeQty(ev) {
    if (parseInt(ev.target.value) > 1) {
      this.loading_message = "Loading..."
      var val = parseInt(ev.target.value);

      this.apiService.getZermemoRates(val).subscribe((data: any) => {

        this.rate = data.detail.rate;
        this.mrp = data.detail.mrp;
        this.discount = data.detail.discount;
        this.delivery_date = data.detail.delivery_date;

        var sub_total = val * data.detail.rate;
      //  var amount_discount = sub_total * data.detail.discount / 100;

        this.amount = sub_total;
        this.loading_message = "";
      });
    }
  }


  ngOnInit() {

  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
