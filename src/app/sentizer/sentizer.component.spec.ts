import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentizerComponent } from './sentizer.component';

describe('SentizerComponent', () => {
  let component: SentizerComponent;
  let fixture: ComponentFixture<SentizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentizerComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
