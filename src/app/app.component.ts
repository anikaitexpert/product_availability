import { Component } from '@angular/core';

import { Platform, AlertController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private oneSignal: OneSignal,
    private splashScreen: SplashScreen,
    private navController: NavController,
    private alertCtrl: AlertController,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#fff');
      //this.statusBar.styleBlackOpaque();
       //this.statusBar.styleLightContent();
    
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    if (this.platform.is('cordova')) {
      this.setupPush();
    }
  }

  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit('ea8dcc94-1bd8-49c7-a35f-c508639f5740', '1053281694195');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);


    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe(data => {
      // Just a note that the data is a different place here!
      let additionalData = data.notification.payload.additionalData;

      this.navController.navigateForward('/notification/' + additionalData.id);

      // this.showAlert('Notification opened', 'You already read this before', additionalData.tip_type);
    });

    this.oneSignal.endInit();
  }

  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    })
    alert.present();
  }
}
