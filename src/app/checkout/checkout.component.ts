import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {

  carts;
  newcarts = [];
  user: any;
  user_id: any;
  orderConfirm: boolean;

  totalGST = 0;
  grandTotal = 0;
  totalQty = 0;
  subtotal = 0;

  totalGST1;
  grandTotal1;
  subtotal1 = 0;


  courier = "";
  note = "";

  outstanding = 0;
  overdue = 0;
  discount = 0;
  instant = 0;
  flag = 0;
  //SBL, SOLTECH division - 15, 16

  //speciality division 

  cat1 = 0;
  cat11 = 0;
  cat2 = 0;
  cat21 = 0;
  cat22 = 0;
  cat23 = 0;
  todaydate = "";
  usercats = [];

  totaldiscount = 0;
  finalgst = 0;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public alertControler: AlertController,
    public commonService: CommonService) {



    this.orderConfirm = false;

    var date = new Date();
    this.todaydate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;

      this.apiService.getUserDetail(this.user_id).subscribe((data: any) => {
        this.outstanding = data.detail.outstanding;
        this.overdue = data.detail.over_due;
      });

    }

    this.localService.getCartItems().then((val) => {

      this.carts = val;
      var finaltotalgst = 0;
      var mycounter = 0;

      this.apiService.getUserCats(this.user_id).subscribe((data89: any) => {
        this.usercats = data89.detail;

        console.log(this.usercats);

        this.carts.forEach(element => {
          mycounter++;
          var single = (parseFloat(element.price) * parseFloat(element.gst)) / 100;
          var gstamount = single * element.count;

          this.totalQty += element.count;
         
          var total = (parseFloat(element.price) * parseFloat(element.count)) + gstamount;
          var s_t = (parseFloat(element.price) * parseFloat(element.count));
              
          if (date.getDate() <= 31 && (date.getMonth() + 1) <= 12 && date.getFullYear() == 2021) {

            var ddscount = this.checkUserCats(this.usercats, element.division_id);
            if (ddscount != "0") {
              element.discount = parseInt(ddscount);        
            }
            else {
              element.discount = 0.5;
            }
          }
          
          element.subtotal = s_t - (s_t * element.discount) / 100;
          this.totalGST += (element.subtotal * parseFloat(element.gst)) / 100;
          this.totaldiscount += (s_t * element.discount) / 100;
          element.subtotal1 = element.subtotal + (element.subtotal * parseFloat(element.gst)) / 100
          this.subtotal += element.subtotal;       
          this.grandTotal += (element.subtotal1);
          this.newcarts.push(element);
        });

      });
      
    });
  }



  checkUserCats(usercats, div_id) {
    var val ="";
    usercats.forEach(element => {     

      if (element.catID == div_id) {              
        val = element.offer;
      }

    });
    return val;
  }

  totaldiscountfun(subtotal, discount) {
    return subtotal + discount;
  }

  changeQty(ev, id) {
    if (parseInt(ev.target.value) > 1) {
      this.carts[id].count = ev.target.value;
    }
    else {
      this.carts[id].count = 1;
    }
  }

  fixVal(val) {
    return val.toFixed(2);
  }

  udpatecart(productlist, id) {
    var qty = parseInt(this.carts[id].count);
    var productPrice = qty * parseFloat(productlist.price);

    let cartProduct = {
      product_id: productlist.product_id,
      name: productlist.name,
      count: qty,
      price: productlist.price,
      totalPrice: productPrice,
    };
    this.localService.addToCart(cartProduct).then((val) => {
      this.localService.getCartItems().then((val) => {
        console.log(val);
      });
    });
  }

  removecart(item) {
    this.localService.removeFromCart(item).then(data => {

      this.localService.getCartItems().then((val) => {
        this.carts = val;
      });

    });
  }

  decreaseProductCount(id) {
    if (this.carts[id].count > 1) {
      this.carts[id].count = parseInt(this.carts[id].count) - 1
    }

  }

  incrementProductCount(id) {
    this.carts[id].count = parseInt(this.carts[id].count) + 1

  }

  ngOnInit() {

  }

  async checkout() {
    //    console.log(this.newcarts)
    if (this.carts.length > 0 && this.orderConfirm == false) {

      if (this.courier != "") {


        const alert = await this.alertControler.create({
          header: 'Please confirm before proceed',
          message: 'Do you want to Continue?',
          buttons: [
            {
              text: 'Yes',
              handler: () => {

                this.apiService.checkoutSubmit({ "cart": this.newcarts, "user_id": this.user_id, "courier": this.courier, "note": this.note, "new": 1, "grandtotal": this.grandTotal, "instant": this.instant }).subscribe((data: any) => {
                  if (data.status == 1) {
                    this.localService.removeAllCartItems();
                    this.apiService.cartObj.next(0);
                    this.commonService.presentAlert('Success', "Thanks For Submitting Your Valuable Order To Solace Biotech Limited");
                    this.orderConfirm = true;
                    this.navController.navigateRoot('/home');
                  }
                  else {

                    this.commonService.presentAlert('Error', data.error);
                  }
                });

              }
            },
            {
              text: 'No',
              handler: () => {
                alert.dismiss();
              }
            }
          ]
        });
        alert.present();

      }
      else {
        this.commonService.presentAlert('Error', "Please select courier agency");
      }

    }
    else {
      this.commonService.presentAlert('Error', "Your cart is empty");
    }
  }

  getTotal(cart) {
    var single = (parseFloat(cart.price) * parseFloat(cart.gst)) / 100;
    var gstamount = single * Number(cart.count);
    var total = (cart.price * cart.count) + gstamount;
    return total.toFixed(2);
  }


  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
