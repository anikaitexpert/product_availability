import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  categories: any;
  submitted = false;
  public registerForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  user: any;
  username = "";
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {


    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.username = this.user.name;
    }

    this.registerForm = this.formBuilder.group({
      id: new FormControl(this.user.id),
      name: new FormControl(this.user.name, Validators.required),
      email: new FormControl(this.user.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      phone: new FormControl(this.user.phone, Validators.compose([Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(10),Validators.maxLength(10)])),
      city: new FormControl(this.user.city, Validators.required),
      state: new FormControl(this.user.state, Validators.required),
     
    });

  }

  get f() { return this.registerForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    console.log(form.value)
    this.apiService.updateUser(form.value).subscribe((data: any) => {
     
      if (data.status == 1) {
        this.loading_message = "Profile Updated";
        this.localService.setUser(data.detail);
        setTimeout(x => {
          this.navController.pop();
        }, 2000);
      }
      else {
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  
  ngOnInit() {

  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
