import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-helpyoudealconfirm',
  templateUrl: './helpyoudealconfirm.component.html',
  styleUrls: ['./helpyoudealconfirm.component.scss'],
})
export class HelpyoudealconfirmComponent implements OnInit {

  product_id:any;
  qty:any;
  price:any;
  productname = "";
  product:any;
  gst:any;
  gstamount:any;
  single:any;
  total:any;
  subtotal:any;
  franchisePrice:any;
  user:any;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
     
      
      }
     }

  ngOnInit() {
    this.qty = localStorage.getItem('selected_qty');
    this.product_id = localStorage.getItem('selected_product_id');

    this.apiService.getSingleProduct(this.product_id).subscribe((data: any) => {     
      this.product = data.detail;
      this.productname = this.product.productName;
      this.price = this.product.new_price;
      this.gst = parseFloat(this.product.GST);
      this.franchisePrice = this.product.franchisePrice;
      this.subtotal = ((parseFloat(this.price) * parseFloat(this.qty))).toFixed(2);
      this.single = (parseFloat(this.price) * parseFloat(this.gst)) / 100;
      this.gstamount = (this.single * Number(this.qty)).toFixed(2);
      this.total = (parseFloat(this.price) * parseFloat(this.qty)) + Number(this.gstamount);

    });

  }

  submit()
  {
    this.apiService.helpyoudealbooking({ "qty": this.qty, "product_id": this.product_id, "user_id": this.user.id, "email": this.user.email}).subscribe((data: any) => {
      if (data.status == 1) {
        this.commonService.presentAlert('Success', "Booking Done.");        
        this.navController.pop();
      }
      else {
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
