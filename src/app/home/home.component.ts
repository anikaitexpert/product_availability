import { Component, OnInit, Renderer2 } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };
  registration = true;
  cartCount = 0;
  club_membership = 0;
  user: any;
  app_version = "0.0.27";
  latest_version = "0.0.27";
  constructor(
    private platform: Platform,
    private router: Router,
    public localService: LocalService,
    public iab: InAppBrowser,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService,
    private renderer2: Renderer2) {
    if (this.localService.isLoggedIn()) {
      this.registration = false;
      this.user = this.localService.getUser();
      if (this.user.club_membership == 1) {
        this.club_membership = 1;
      }

    }
    this.apiService.cartObj.subscribe((data) => {
      this.localService.getCartItems().then((val1) => {

        if (val1 != null)
          this.cartCount = val1.length;
        else
          this.cartCount = 0;

      });
    });
  }
  ngOnInit() {

    
    this.apiService.check_app_version().subscribe((data: any) => {
      this.latest_version = data.version;     
    });

    this.renderer2.removeClass(document.body, 'inner-class');

    this.apiService.userObj.subscribe((data) => {
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        if (this.user.club_membership == 1) {
          this.club_membership = 1;
        } else {
          this.club_membership = 0;
        }
      }

      if (data != null) {
        if (this.localService.isLoggedIn()) {
          this.registration = false;
        }
        else {
          this.registration = true;
          this.club_membership = 0;
        }
      }
    });

    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.router.url === '/home') {
        navigator['app'].exitApp();
      } else {
        this.navController.pop();
      }
    });

  }

  toggleMenu() {
    const splitPane = document.querySelector('ion-split-pane');
    const windowWidth = window.innerWidth;
    const splitPaneShownAt = 992;
    const when = `(min-width: ${splitPaneShownAt}px)`;
    if (windowWidth >= splitPaneShownAt) {
      // split pane view is visible
      const open = splitPane.when === when;
      splitPane.when = open ? false : when;
    } else {
      // split pane view is not visible
      // toggle menu open
      const menu = splitPane.querySelector('ion-menu');
      return menu.open();
    }
  }

  aboutus() {
    this.navController.navigateForward('/about-us');
  }
  cart() {

    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/cart');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }

  jfc() {

    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/jfc');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }

  productlist(id, logo) {
    // this.navController.navigateForward('/product-list/' + id + "/" + logo);
  }
  products() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/products');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  clubdivisions() {
    if (this.localService.isLoggedIn()) {
      if(this.club_membership == 1)
      {
      this.navController.navigateForward('/clubform-list');
      }
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  notifications() {
    this.navController.navigateForward('/notifications');
  }
  myorders() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/orders');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  offers() {
    this.navController.navigateForward('/rewards');
  }
  profile() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/profile');
    } else {
      this.navController.navigateForward('/login')
    }
  }

  booking()
  {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/booking');
    } else {
      this.navController.navigateForward('/login')
    }   
  }

  helpyoudeal()
  {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/helpyoudeal');
    } else {
      this.navController.navigateForward('/login')
    }
  }

  sentizer() {
    this.navController.navigateForward('/sentizer');
  }
  visualaids() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/visual-aids');
    } else {
      this.navController.navigateForward('/login')
    }
  }
  contactus() {
    this.navController.navigateForward('/contactus');
  }
  login() {
    this.navController.navigateForward('/login');
  }

  OpenFacebook() {
    const browser = this.iab.create('http://www.facebook.com/solacebiotechlimited');
    browser.show()
  }

  OpenTwitter() {
    const browser = this.iab.create('https://twitter.com/solacebiotech');
    browser.show()
  }

  OpenLinkedIn() {
    const browser = this.iab.create('https://www.linkedin.com/in/solacebiotechlimited/');
    browser.show()
  }
}
