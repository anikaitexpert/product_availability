import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bookingconfirm',
  templateUrl: './bookingconfirm.component.html',
  styleUrls: ['./bookingconfirm.component.scss'],
})
export class BookingconfirmComponent implements OnInit {
  bookingid:any;
  bookingdetail:any;
  flag = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public activatedRoute:ActivatedRoute,
    public commonService: CommonService) {
      this.bookingid = this.activatedRoute.snapshot.params.id;

      this.apiService.getConfirmationBooking(this.bookingid).subscribe((data: any) => {
        this.bookingdetail = data.detail;
        this.flag =true;
        console.log(data.detail);
      });
    }
    
    multifun(req,val)
    {
      return req*Number(val.replace(/[^0-9\.]+/g,""));
    }
    myperfun(req,val)
    {
      return ((12*(req*Number(val.replace(/[^0-9\.]+/g,""))))*0.25);
    }
  ngOnInit() {}

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
