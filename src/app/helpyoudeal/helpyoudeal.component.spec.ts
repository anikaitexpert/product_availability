import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpyoudealComponent } from './helpyoudeal.component';

describe('HelpyoudealComponent', () => {
  let component: HelpyoudealComponent;
  let fixture: ComponentFixture<HelpyoudealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpyoudealComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpyoudealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
