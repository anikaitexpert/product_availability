import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-helpyoudeal',
  templateUrl: './helpyoudeal.component.html',
  styleUrls: ['./helpyoudeal.component.scss'],
})
export class HelpyoudealComponent implements OnInit {
  shownGroup = null;
  categoryId;
  productLists = [];
  allData = [];
  loading_message = "Loading...";
  loading = false;
  selectProduct: any;
  productCount: number = 1;


  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

  }

  ngOnInit() {
    this.apiService.getHYDPProducts().subscribe((data: any) => {
      this.productLists = data.detail;

      var i = 0;
      this.productLists.forEach(element => {
        this.productLists[i].btnname = "Apply Help You Deal";
        this.productLists[i].offerflag = false;
        i++;
      });

    });
    console.log(this.productLists);
  }

  addToCart(element, i) {
    if (this.productLists[i].offerflag == false) {
      this.productLists[i].btnname = "Book Now"
      this.productLists[i].offerflag = true;
    } else {
      if(this.productLists[i].tmp == 0)
      {
        this.commonService.presentAlert('Alert',"Please Enter Quantity");
      }
      else
      {
        localStorage.setItem('selected_qty',this.productLists[i].tmp);
        localStorage.setItem('selected_product_id',this.productLists[i].id);
        this.navController.navigateForward('/helpyoudealconfirm');
      }
    }
  }

  decreaseProductCount(id) {
    if (this.productLists[id].tmp > 1) {
      this.productLists[id].tmp = parseInt(this.productLists[id].tmp) - 1
    }

  }

  incrementProductCount(id) {
    this.productLists[id].tmp = parseInt(this.productLists[id].tmp) + 1

  }

  changeQty(ev, id) {
    if( parseInt(ev.target.value) > 0)
    this.productLists[id].tmp = ev.target.value;
  }


  toggleGroup(group) {
    this.productCount = 1;
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };


  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
