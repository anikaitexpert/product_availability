import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jfcsummary',
  templateUrl: './jfcsummary.component.html',
  styleUrls: ['./jfcsummary.component.scss'],
})
export class JfcsummaryComponent implements OnInit {
  jfc_id:any;
  artworkmessages = [];
  ppmmessages = [];
  apimessages = [];
  exicipentsmessages = [];
  distributionsmessages = [];
  posmessages = [];
  jfcdetail:any;
  loading = false;
  artworkstatus = 0;
  ppmstatus = 0;
  apistatus = 0;
  distributionstatus = 0;
  postatus = 0;
  exicipentstatus=0;
  product_name = "";
  quantity = "";
  brand_name = "";
  order_value= "";

  constructor(public localService: LocalService,
    public apiService: ApiService,
    public activatedRoute:ActivatedRoute,
    private navController: NavController,
    public commonService: CommonService) { 
      this.jfc_id = this.activatedRoute.snapshot.params.id;

    }

  ngOnInit() {
    this.loading = true;
    
    this.apiService.getJFCMessages(this.jfc_id).subscribe((data: any) => {    
      this.loading = false;  
      this.jfcdetail = data;

      this.product_name = data.jfc_summary.product_name;
      this.brand_name = data.jfc_summary.brand_name;
      this.quantity = data.jfc_summary.quantity;
      this.order_value = data.jfc_summary.order_value;

      this.artworkstatus = data.artwork.status;
      this.ppmstatus = data.ppm_box.status;
      this.apistatus = data.api.status;
      this.distributionstatus = data.distribution.status;
      this.postatus = data.po.status;
      this.exicipentstatus = data.excipients.status;

      data.detail.forEach(element => {
        if(element.department_id == 1)
        {
          this.artworkmessages.push(element);
        }
        else if(element.department_id == 2)
        {
          this.ppmmessages.push(element);
        }
        else if(element.department_id == 3)
        {
          this.apimessages.push(element);
        }
        else if(element.department_id == 4)
        {
          this.exicipentsmessages.push(element);
        }
        else if(element.department_id == 5)
        {
          this.distributionsmessages.push(element);
        }
        else if(element.department_id == 6)
        {
          this.posmessages.push(element);
        }
        
      });

    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

}