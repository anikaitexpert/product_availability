import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  loading_message = "Loading...";
  loading = false;
  categories:any;
  user: any;
  user_id: any;
  dateModified = "Loading..."
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
      }
  }

 async ngOnInit() {
    this.loading = true;
    this.apiService.getProducts(this.user_id).subscribe((data: any) => {    
      this.loading = false;  
      this.categories = data.detail;

    });

    this.apiService.getLastStockUpdate().subscribe((data: any) => {    
      this.loading = false;  
      this.dateModified = data.detail.dateModified;
    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  opencart()
  {
    this.navController.navigateForward('/cart');
  }

  productlist(id,catname)
  {
    this.navController.pop();
    this.navController.navigateForward('/product-list/'+id+"/"+catname);
  }
}
