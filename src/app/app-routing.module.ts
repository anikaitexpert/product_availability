import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RegisterComponent } from './register/register.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { ReferralComponent } from './referral/referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RewardsComponent } from './rewards/rewards.component';
import { HelpComponent } from './help/help.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrdersComponent } from './orders/orders.component';
import { ClubdivisionComponent } from './clubdivision/clubdivision.component';
import { ClubformlistComponent } from './clubformlist/clubformlist.component';
import { Clubform1Component } from './clubform1/clubform1.component';
import { Clubform2Component } from './clubform2/clubform2.component';
import { Clubform3Component } from './clubform3/clubform3.component';
import { Clubform4Component } from './clubform4/clubform4.component';
import { NotificationComponent } from './notification/notification.component';
import { Clubform5Component } from './clubform5/clubform5.component';
import { VisualAidsComponent } from './visual-aids/visual-aids.component';
import { VisualAssignComponent } from './visual-assign/visual-assign.component';
import { VisualsComponent } from './visuals/visuals.component';
import { SentizerComponent } from './sentizer/sentizer.component';
import { JfcComponent } from './jfc/jfc.component';
import { JfcsummaryComponent } from './jfcsummary/jfcsummary.component';
import { BookingComponent } from './booking/booking.component';
import { BookingconfirmComponent } from './bookingconfirm/bookingconfirm.component';
import { HelpyoudealComponent } from './helpyoudeal/helpyoudeal.component';
import { HelpyoudealconfirmComponent } from './helpyoudealconfirm/helpyoudealconfirm.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'product-list/:id/:catName', component: ProductListComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'login', component: LoginComponent },
      { path: 'referral', component: ReferralComponent },
      { path: 'add-referral', component: AddReferralComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'contactus', component: ContactusComponent },
      { path: 'notifications', component: NotificationsComponent },
      { path: 'notification/:id', component: NotificationComponent },
      { path: 'rewards', component: RewardsComponent },
      { path: 'help', component: HelpComponent },
      { path: 'cart', component: CartComponent },
      { path: 'checkout', component: CheckoutComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'clubdivision', component: ClubdivisionComponent },
      { path: 'clubform-list', component: ClubformlistComponent },
      { path: 'clubform1', component: Clubform1Component },
      { path: 'clubform2', component: Clubform2Component },
      { path: 'clubform3', component: Clubform3Component },
      { path: 'clubform4', component: Clubform4Component },
      { path: 'clubform5', component: Clubform5Component },
      { path: 'visual-aids', component: VisualAidsComponent },
      { path: 'visual-assign/:id/:doctorName', component: VisualAssignComponent },
      { path: 'visuals/:id/:doctorName', component: VisualsComponent },
      { path: 'sentizer', component: SentizerComponent },
      { path: 'jfc', component: JfcComponent },
      { path: 'jfcsummary/:id', component: JfcsummaryComponent },
      { path: 'booking', component: BookingComponent },
      { path: 'bookingconfirm/:id', component: BookingconfirmComponent },
      { path: 'helpyoudeal', component: HelpyoudealComponent },
      { path: 'helpyoudealconfirm', component: HelpyoudealconfirmComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
