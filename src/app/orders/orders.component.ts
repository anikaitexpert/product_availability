import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {

  orders = [];
  user: any;
  user_id: any;

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {

    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;
    } 
  }

  ngOnInit() {
    this.apiService.getOrders(this.user_id).subscribe((data: any) => {    
      this.orders = data.detail;

    });
  }


  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
