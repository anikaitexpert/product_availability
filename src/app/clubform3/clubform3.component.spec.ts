import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Clubform3Component } from './clubform3.component';

describe('Clubform3Component', () => {
  let component: Clubform3Component;
  let fixture: ComponentFixture<Clubform3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clubform3Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Clubform3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
