import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  carts;
  user:any;
  user_id:any;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {

      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;       
      }

    this.localService.getCartItems().then((val) => {
      this.carts = val;
      console.log(this.carts);
    });
   
  }

  changeQty(ev,id)
  {
    if (parseInt(ev.target.value) > 1) {
    this.carts[id].count = ev.target.value;
    }
    else
    {
      this.carts[id].count = 1;
    }
  }

  udpatecart(productlist,id) {    
    var qty = parseInt(this.carts[id].count);
    var productPrice = qty * parseFloat(productlist.price);
    let cartProduct = {
      product_id: productlist.product_id,
      division_id: productlist.proCat,
      name: productlist.name,
      count: qty,      
      gst: productlist.gst,
      price: parseFloat(productlist.price).toFixed(2),
      totalPrice: productPrice,
    };
    this.localService.addToCart(cartProduct).then((val) => {  
      this.localService.getCartItems().then((val) => {
        let cartCount = JSON.stringify(val.length);      
       this.apiService.cartObj.next(cartCount);
      }); 
    });
    this.commonService.presentAlert('Success',"Cart is updated");
  }

  removecart(item)
  {
    this.localService.removeFromCart(item).then(data => {

      this.localService.getCartItems().then((val) => {
        this.carts = val;
        let cartCount = JSON.stringify(val.length);      
        this.apiService.cartObj.next(cartCount);
      });

    });
  }

  decreaseProductCount(id) {
    if (this.carts[id].count > 1) {
      this.carts[id].count = parseInt(this.carts[id].count) - 1
    }

  }

  incrementProductCount(id) {
    this.carts[id].count = parseInt(this.carts[id].count) + 1

  }

  ngOnInit() { }

  checkout()
  {
    this.navController.navigateForward('/checkout');
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
