import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss'],
})
export class ContactusComponent implements OnInit {
  categories: any;
  submitted = false;
  public registerForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  user: any;
  username = "";
  emailvalid = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {


    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.username = this.user.full_name;
    }

    this.registerForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([Validators.required,Validators.pattern('^[A-Za-z? ]+$')])),
      email: new FormControl('', Validators.compose([
        Validators.required
//        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      mobile: new FormControl('',  Validators.compose([Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(10),Validators.maxLength(10)])),
      query: new FormControl('', Validators.required),
     
    });

  }

  get f() { return this.registerForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    console.log(form.value)
    this.apiService.contactSubmit(form.value).subscribe((data: any) => {
    
      if (data.status == 1) {
        this.loading_message = "Query submitted successfully.";
        this.commonService.presentAlert('Info',"Query submitted successfully.");   
      }
      else {
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  checkEmail(ev)
  {
    console.log(ev.target.value);
    if(ev.target.value != "")
    {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test(ev.target.value) == false)
      {
         this.emailvalid = true;
      }
      else
      {
        this.emailvalid = false;
      }
    }else
    {
      this.emailvalid = false;
    }
  }

  
  ngOnInit() {

  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
