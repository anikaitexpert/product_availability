import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-add-referral',
  templateUrl: './add-referral.component.html',
  styleUrls: ['./add-referral.component.scss'],
})
export class AddReferralComponent implements OnInit {
  submitted = false;
  user: any;
  userId = 0;
  public addReferralForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  mydata = {'full_name':"vikas","email":"saini391@gmail.com","mobile":"9837483749"};

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.userId = this.user.id;
    }

    this.addReferralForm = this.formBuilder.group({
      full_name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.required),
    });


  }
  get f() { return this.addReferralForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.addReferralForm.invalid) {
      return;
    }
   
    this.loading = true;

    this.apiService.addReferral(form.value.full_name,form.value.email,form.value.mobile, this.userId).subscribe((data: any) => {
     
      if (data.status == 1) {
        this.loading_message = "Inserted";   
        this.commonService.presentAlert('Info',"Inserted");   
        setTimeout(x => {
          this.navController.pop();
        }, 2000);
      }
      else {
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  ngOnInit() { }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
