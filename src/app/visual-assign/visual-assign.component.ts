import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-visual-assign',
  templateUrl: './visual-assign.component.html',
  styleUrls: ['./visual-assign.component.scss'],
})
export class VisualAssignComponent implements OnInit {
  id;
  doctorName
  mydata:any;
  categories:any;
  category_id:any;
  total_selected_category = 0;
  data = {
    user_id:"",
    doctor_id:"",
    category_id:"",
    categories:""
  };
  user:any;
  user_id:any;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    this.id = this.activatedRoute.snapshot.params.id;
    this.doctorName = this.activatedRoute.snapshot.params.doctorName;
    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;
    }
    this.apiService.getCategories().subscribe((data: any) => {
      this.categories = data.detail;
    });
  }

  getVisualAids(value)
  {
    this.category_id = value;
    this.apiService.getVisualAids(value, this.user_id, this.id).subscribe((data: any) => {
      this.mydata = data.detail;
     
    });
  }
  
  selectMember(data){
    this.total_selected_category = 0;
    data.checked = !data.checked;
     console.log(this.mydata);

     this.mydata.forEach(element => {
       if(element.checked === false)
       {
          this.total_selected_category++;
       }
     });
    
   }

   submitdata()
   {
    this.data.user_id = this.user_id;  
    this.data.doctor_id = this.id;  
    this.data.category_id = this.category_id;  
    this.data.categories = this.mydata;
    this.apiService.addVisualAids(this.data).subscribe((data: any) => {
      if (data.status == 1) {
        this.commonService.presentAlert("Success", "Submitted successfully.");
      }
      
    });
   }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  ngOnInit() {}

}
