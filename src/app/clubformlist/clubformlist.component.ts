import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-clubformlist',
  templateUrl: './clubformlist.component.html',
  styleUrls: ['./clubformlist.component.scss'],
})
export class ClubformlistComponent implements OnInit {
  shownGroup = null;
  categoryId;
  loading_message = "Loading...";
  loading = false;
  brandName;
  cartCount = 0;

  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
  //  this.categoryId = this.activatedRoute.snapshot.params.id;
   // this.brandName = this.activatedRoute.snapshot.params.catName;

    this.apiService.cartObj.subscribe((data) => {
      this.localService.getCartItems().then((val1) => {       
        if(val1 != null)      
        this.cartCount = val1.length;
        else
        this.cartCount = 0;
       });
    });
  }
  async ngOnInit() {
    
  }

  clubform1()
  {
    this.navController.navigateForward('/clubform1');
  }

  opencart()
  {
    this.navController.navigateForward('/cart');
  }
  clubform2()
  {
    this.navController.navigateForward('/clubform2');
  }
  clubform3()
  {
    this.navController.navigateForward('/clubform3');
  }
  clubform4()
  {
    this.navController.navigateForward('/clubform4');
  }
  clubform5()
  {
    this.navController.navigateForward('/clubform5');
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

}
