import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  shownGroup = null;
  categoryId;
  productLists = [];
  allData = [];
  loading_message = "Loading...";
  loading = false;
  selectProduct: any;
  productCount: number = 1;
  cartItems: any[];
  qtd: any[];
  brandName;
  cartCount = 0;
  searchTerm = "";

  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    this.categoryId = this.activatedRoute.snapshot.params.id;
    this.brandName = this.activatedRoute.snapshot.params.catName;

    this.apiService.cartObj.subscribe((data) => {
      this.localService.getCartItems().then((val1) => {       
        if(val1 != null)      
        this.cartCount = val1.length;
        else
        this.cartCount = 0;
       });
    });
  }
  async ngOnInit() {
    this.loading = true;
    this.apiService.getProductList(this.categoryId).subscribe((data: any) => {
      this.loading = false;
      this.productLists = data.detail;
      this.allData =  data.detail;
    });

  }

  filterItems(searchTerm) {
    console.log(searchTerm);
    this.productLists =  this.allData.filter(item => {
      return item.productName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  decreaseProductCount(id) {
    if (this.productLists[id].tmp > 1) {
      this.productLists[id].tmp = parseInt(this.productLists[id].tmp) - 1
    }

  }

  incrementProductCount(id) {
    this.productLists[id].tmp = parseInt(this.productLists[id].tmp) + 1

  }

  addToCart(productlist, id) {
    var qty = parseInt(this.productLists[id].tmp);
    var productPrice = qty * parseFloat(productlist.franchisePrice);

    if(qty == 0)
    return false;

    let cartProduct = {
      product_id: productlist.id,
      division_id: productlist.proCat,
      name: productlist.productName,
      count: qty,
      gst: parseFloat(productlist.GST),
      price: parseFloat(productlist.franchisePrice).toFixed(2),
      totalPrice: productPrice,
    };
    this.localService.addToCart(cartProduct).then((val) => {
      this.localService.getCartItems().then((val1) => {       
       let cartCount = JSON.stringify(val1.length);      
       this.apiService.cartObj.next(cartCount);
      });
    });
    this.commonService.presentAlert('Success',"Item inserted into cart");
 
  }

  opencart() {
    this.navController.navigateForward('/cart');
  }
  changeQty(ev, id) {
    if( parseInt(ev.target.value) > 0)
    this.productLists[id].tmp = ev.target.value;
  }

  toggleGroup(group) {
    this.productCount = 1;
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  enquiry() {
    this.navController.navigateForward('/contactus');
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
