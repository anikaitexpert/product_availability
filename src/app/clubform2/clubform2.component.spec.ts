import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Clubform2Component } from './clubform2.component';

describe('Clubform2Component', () => {
  let component: Clubform2Component;
  let fixture: ComponentFixture<Clubform2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clubform2Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Clubform2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
