import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
@Component({
  selector: 'app-clubform2',
  templateUrl: './clubform2.component.html',
  styleUrls: ['./clubform2.component.scss'],
})
export class Clubform2Component implements OnInit {
  user:any;
  user_id:any;
  lists = [];
  monthsdata: any;
  date: any;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) { 
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
       }

       this.apiService.getClubMembershipList().subscribe((data: any) => {

        if (data.status == 1) {
          this.monthsdata = data.detail;
  
          data.detail.forEach(element => {
            if (element.status == 1) {
              this.date = element.date;
              this.loadclub(this.user_id, this.date);
            }         
          });       
        }
      });
      }

  ngOnInit() {
   
  }
  getmonthwise() {
    this.loadclub(this.user_id, this.date);
  }
  loadclub(uid,dt)
  {
    this.apiService.clubform2(uid,dt).subscribe((data: any) => {
          
      if (data.status == 1) {
        this.lists = data.detail;
      }
    });
  }
  clubformSubmit(item)
  {
    this.apiService.clubform2Submit(item.id).subscribe((data: any) => {
          
      if (data.status == 1) {
        this.commonService.presentAlert("success","Thank submitting for Navratna/Panchratna Claim Form Submission");
        this.loadclub(this.user_id,this.date);
      }
    });
  }
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
