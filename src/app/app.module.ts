import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RegisterComponent } from './register/register.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { ReferralComponent } from './referral/referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RewardsComponent } from './rewards/rewards.component';
import { HelpComponent } from './help/help.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrdersComponent } from './orders/orders.component';
import { ClubdivisionComponent } from './clubdivision/clubdivision.component';
import { ClubformlistComponent } from './clubformlist/clubformlist.component';
import { Clubform1Component } from './clubform1/clubform1.component';
import { Clubform2Component } from './clubform2/clubform2.component';
import { Clubform3Component } from './clubform3/clubform3.component';
import { Clubform4Component } from './clubform4/clubform4.component';
import { Clubform5Component } from './clubform5/clubform5.component';
import { VisualAidsComponent } from './visual-aids/visual-aids.component';
import { VisualAssignComponent } from './visual-assign/visual-assign.component';
import { VisualsComponent } from './visuals/visuals.component';
import { NotificationComponent } from './notification/notification.component';
import { SentizerComponent } from './sentizer/sentizer.component';
import { JfcComponent } from './jfc/jfc.component';
import { JfcsummaryComponent } from './jfcsummary/jfcsummary.component';
import { BookingComponent } from './booking/booking.component';
import { BookingconfirmComponent } from './bookingconfirm/bookingconfirm.component';
import { HelpyoudealComponent } from './helpyoudeal/helpyoudeal.component';
import { HelpyoudealconfirmComponent } from './helpyoudealconfirm/helpyoudealconfirm.component';

import { ApiService } from './service/api.service';
import { LocalService } from './service/local.service';
import { CommonService } from './service/common.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    AboutUsComponent,
    RegisterComponent,
    ProductListComponent,
    ProductsComponent,
    LoginComponent,
    ReferralComponent,
    AddReferralComponent,
    ProfileComponent,
    ContactusComponent,
    NotificationsComponent,
    RewardsComponent,
    HelpComponent,
    CartComponent,
    CheckoutComponent,
    OrdersComponent,
    ClubdivisionComponent,
    ClubformlistComponent,
    Clubform1Component,
    Clubform2Component,
    Clubform3Component,
    Clubform4Component,
    NotificationComponent,
    Clubform5Component,
    VisualAidsComponent,
    VisualAssignComponent,
    VisualsComponent,
    SentizerComponent,
    JfcComponent,
    JfcsummaryComponent,
    BookingComponent,
    BookingconfirmComponent,
    HelpyoudealComponent,
    HelpyoudealconfirmComponent
  ],
  entryComponents: [],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService, 
    LocalService,
    CommonService,
    InAppBrowser,
    OneSignal
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
