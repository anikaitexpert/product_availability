import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
})
export class BookingComponent implements OnInit {

  submitted = false;
  public registerForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  loading2 = false;
  categories = [];
  products = [];
  user:any;
  username:any;
  email:any;
  company_name:any;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        console.log(this.user);
        this.username = this.user.name;
        this.email = this.user.email;
        this.company_name = this.user.companyName;
      }

    this.registerForm = this.formBuilder.group({
      solmate_name: new FormControl(this.username, Validators.required),
      company_name: new FormControl(this.company_name, Validators.required),      
      email: new FormControl(this.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      category_id: new FormControl('', Validators.required),
      product_range: new FormControl('', Validators.required),
      requirement: new FormControl('', Validators.required),
    });

  }

  get f() { return this.registerForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;

    this.apiService.booking({ "solmate": form.value.solmate_name, "company_name": form.value.company_name,"email": form.value.email, "product_range": form.value.product_range, "requirement": form.value.requirement }).subscribe((data: any) => {
      if (data.status == 1) {
        console.log(data);
        this.loading_message = "Booking Successful.";
        this.commonService.presentAlert('Success', "Booking Successfully.");
        this.navController.navigateForward('/bookingconfirm/'+data.detail);
      }
      else {
        this.loading = false;
        this.commonService.presentAlert('Error', data.error);
      }
    });

  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  ngOnInit() {
    this.apiService.getBookingCategories().subscribe((data: any) => {
      this.categories = data.detail;
    
      console.log(data.detail);
    });
  }

  checkValue(id)
  {
    console.log(id);
    this.apiService.getBookingSubcategories(id).subscribe((data: any) => {
      this.products = data.detail;      
      console.log(data.detail);
    });
  }
}
