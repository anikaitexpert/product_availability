import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
  shownGroup = null;
  notificationid: any;
  user: any;
  user_id: any;
  notification:any;
  loading = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    public activatedRoute:ActivatedRoute,
    private navController: NavController,
    public commonService: CommonService) {

      this.notificationid = this.activatedRoute.snapshot.params.id;
  }


  ngOnInit() {
    this.getNotification(this.notificationid);
  }

  async getNotification(id) {
    this.loading = true;
    this.apiService.getNotification(id).subscribe((data: any) => {
      this.loading = false;
      this.notification = data.detail;
    });
  }

 
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

 
}
