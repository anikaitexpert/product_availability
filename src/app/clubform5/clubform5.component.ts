import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-clubform5',
  templateUrl: './clubform5.component.html',
  styleUrls: ['./clubform5.component.scss'],
})
export class Clubform5Component implements OnInit {
  user:any;
  user_id:any;
  list = [];
  monthsdata: any;
  date: any;
  status = 0;
  submitted = 0;
  position = 0;
  outstanding = 0;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) { 
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
       }
       this.apiService.getClubMembershipList().subscribe((data: any) => {

        if (data.status == 1) {
          this.monthsdata = data.detail;
          
          data.detail.forEach(element => {
            if (element.status == 1) {
              this.date = element.date;
              this.loadclub(this.user_id, this.date);
            }         
          });       
        }
      });
      }

  ngOnInit() {
   
  }
  getmonthwise() {
    this.loadclub(this.user_id, this.date);
  }
  loadclub(uid,dt)
  {
    this.apiService.clubform5(uid,dt).subscribe((data: any) => {
          
      if (data.status == 1) {
        this.status =  data.detail.status;
        this.submitted =  data.detail.submitted;
        this.position =  data.detail.position;
        this.outstanding =  data.detail.outstanding;
      }
    });
  }
  clubformSubmit(item)
  {
    this.apiService.clubform5Submit(item.id).subscribe((data: any) => {
          
      if (data.status == 1) {
        this.commonService.presentAlert("success","Thank submitting for Right To Choose 50 (RTC50) Contest");
        this.loadclub(this.user_id, this.date);
      }
    });
  }
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}