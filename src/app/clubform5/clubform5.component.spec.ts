import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Clubform5Component } from './clubform5.component';

describe('Clubform5Component', () => {
  let component: Clubform5Component;
  let fixture: ComponentFixture<Clubform5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clubform5Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Clubform5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
