import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-visuals',
  templateUrl: './visuals.component.html',
  styleUrls: ['./visuals.component.scss'],
})
export class VisualsComponent implements OnInit {
  id;
  user:any;
  user_id:any;
  doctorName:any;
  visuals:any;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    this.id = this.activatedRoute.snapshot.params.id;
    this.doctorName = this.activatedRoute.snapshot.params.doctorName;

    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;
    }
    this.apiService.getFolderVisuals(this.id).subscribe((data: any) => {
      this.visuals = data.detail;
    });
   
  }
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  ngOnInit() {}

}
