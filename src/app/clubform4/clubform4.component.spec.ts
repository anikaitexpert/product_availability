import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Clubform4Component } from './clubform4.component';

describe('Clubform4Component', () => {
  let component: Clubform4Component;
  let fixture: ComponentFixture<Clubform4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clubform4Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Clubform4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
