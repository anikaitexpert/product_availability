import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';


@Component({
  selector: 'app-visual-aids',
  templateUrl: './visual-aids.component.html',
  styleUrls: ['./visual-aids.component.scss'],
})
export class VisualAidsComponent implements OnInit {
  user: any;
  user_id: any;
  lists = [];
  constructor(private activatedRoute: ActivatedRoute,
    public alertControler: AlertController,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;
      this.loadFolder();
    }
  }

  loadFolder() {
    this.apiService.getFoldersName(this.user_id).subscribe((data: any) => {

      if (data.status == 1) {
        this.lists = data.detail;
      }
    });
  }

  ngOnInit() { }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  addVisualAids(id,doctorName)
  {
    this.navController.navigateForward('/visual-assign/'+id+"/"+doctorName);
  }
  openVisuals(id,doctorName)
  {
    this.navController.navigateForward('/visuals/'+id+"/"+doctorName);
  }
  deleteVisualAids(id)
  {
    this.apiService.deleteVisualAids(id,this.user_id).subscribe((data: any) => {

      if (data.status == 1) {
        this.commonService.presentAlert("Success", "Deleted");
        this.loadFolder();
      }
    });
  }
  async add_doctor() {
    if (event) {
      event.stopPropagation();
    }
    const alert = await this.alertControler.create(
      {
        header: 'Doctor Name',
        subHeader: '',
        inputs: [
          {
            name: 'Doctor',
            placeholder: 'Enter Doctor Name'
          },
        ],
        cssClass: 'alert-subscribe',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              alert.dismiss();
            }
          },
          {
            text: 'Ok',
            handler: (data) => {
              //data.Review
              this.apiService.addFolderName(this.user_id, data.Doctor).subscribe((data: any) => {

                if (data.status == 1) {
                  this.commonService.presentAlert("Success", "Inserted");
                  alert.dismiss();
                  this.loadFolder();
                }
              });

            }
          }
        ]
      });

    await alert.present();
  }
}
