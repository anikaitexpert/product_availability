import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Clubform1Component } from './clubform1.component';

describe('Clubform1Component', () => {
  let component: Clubform1Component;
  let fixture: ComponentFixture<Clubform1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clubform1Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Clubform1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
