import { Injectable } from "@angular/core";
import { AlertController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(public http: HttpClient,public alertController: AlertController, public loadingController: LoadingController) {

  }

  async presentAlert(title, msg) {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    return loading;
  }

  validateEmail(emailField) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(emailField) == false) {
      return false;
    }

    return true;

  }

  validateUsername(value) {
    var reg = /^[a-zA-Z0-9]+$/;

    if (reg.test(value) == false) {
      return false;
    }

    return true;

  }

  getDate(dateObj) {
    var dd = String(dateObj.getDate()).padStart(2, '0');
    var mm = String(dateObj.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = dateObj.getFullYear();

    return yyyy + '-' + mm + '-' + dd;

  }

  getDateOneMonthBack(dateObj) {
    var dd = String(dateObj.getDate()).padStart(2, '0');
    var mm = String(dateObj.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = dateObj.getFullYear();

    return yyyy + '-' + mm + '-' + dd;

  }
}
