import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';

const CART_KEY = 'cartItems';

@Injectable({
  providedIn: 'root'
})
export class LocalService {
  url: string;
  headers: any;
  httpOptions: any;
  userObj = new BehaviorSubject(null);

  constructor(public http: HttpClient,public storage: Storage) {

    this.url = environment.url;

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }


  isLoggedIn() {
    let user = localStorage.getItem('user');
    if (user) {
      return true;
    } else
      return false;
  }

  getUser() {
    let user = localStorage.getItem('user');
    return JSON.parse(user);
  }

  setUser(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  addToCart(product) {
    return this.getCartItems().then(result => {
      if (result) {
        if (!this.containsObject(product, result)) {
          result.push(product);
          return this.storage.set(CART_KEY, result);
        } else {
          let index = result.findIndex(x => x.product_id == product.product_id);
         // let prevQuantity = parseInt(result[index].count);
         // product.count = (prevQuantity + product.count);
          let currentPrice = product.totalPrice;
          product.totalPrice =currentPrice;
           result.splice(index, 1);
          result.push(product);
          return this.storage.set(CART_KEY, result);
        }

      } else {
        return this.storage.set(CART_KEY, [product]);
      }
    })
  }

  removeFromCart(product) {
    return this.getCartItems().then(result => {
      if (result) {
        let index = result.findIndex(x => x.product_id == product.product_id);
        result.splice(index, 1);
        return this.storage.set(CART_KEY, result);
      }
    })
  }

  removeAllCartItems() {
    return this.storage.remove(CART_KEY).then(res => {
      return res;
    });
  }


  containsObject(obj, list): boolean {
    if (!list.length) {
      return false;
    }

    if (obj == null) {
      return false;
    }
    var i;
    for (i = 0; i < list.length; i++) {
      if (list[i].product_id == obj.product_id) {
        return true;
      }
    }
    return false;
  }



  getCartItems() {
    return this.storage.get(CART_KEY);
  }

  logout(){
    // clear the localstorage
    localStorage.clear();   
  }
}
