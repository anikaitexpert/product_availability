import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url: string;
  headers: any;
  httpOptions: any;
  httpOptions2: any;
  userObj = new BehaviorSubject(null);
  registerObj = new BehaviorSubject(null);
  cartObj = new BehaviorSubject(null);


  constructor(public http: HttpClient) {
    this.url = environment.url;
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.httpOptions2 = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
  }

  register(data) {
    return this.http.post(this.url + "signup", data, this.httpOptions);
  }
  checkoutSubmit(data) {
    return this.http.post(this.url + "checkout", data, this.httpOptions);
  }
  updateUser(data) {
    return this.http.post(this.url + "updateUser", data, this.httpOptions);
  }
  contactSubmit(data) {
    return this.http.post(this.url + "contactSubmit?name=" + data.name + "&mobile=" + data.mobile + "&email=" + data.email + "&data=" + data.query, this.httpOptions);
  }
  login(data) {
    return this.http.post(this.url + "login", data, this.httpOptions);
  }

  getProductList(id) {
    return this.http.get(this.url + "getProductList?id=" + id, this.httpOptions);
  }

  getSingleProduct(id)
  {
    return this.http.get(this.url + "getSingleProduct?id=" + id, this.httpOptions);    
  }

  getNotifications() {
    return this.http.get(this.url + "getNotifications", this.httpOptions);
  }

  getNotification(id) {
    return this.http.get(this.url + "getNotification?id=" + id, this.httpOptions);
  }

  getCustomerList(id) {
    return this.http.get(this.url + "getCustomerList?id=" + id, this.httpOptions);
  }

  getRewardsList(id) {
    return this.http.get(this.url + "getRewardsList?id=" + id, this.httpOptions);
  }

  getProducts(user_id) {
    return this.http.get(this.url + "getProducts?user_id=" + user_id, this.httpOptions);
  }
  getCategories() {
    return this.http.get(this.url + "getCategories", this.httpOptions);
  }

  getOrders(user_id) {
    return this.http.get(this.url + "getUserOrders?user_id=" + user_id, this.httpOptions);
  }

  addReferral(name, email, mobile, id) {
    return this.http.get(this.url + "addReferral?id=" + id + "&name=" + name + "&email=" + email + "&mobile=" + mobile);
  }

  clubform1(userId, date) {
    return this.http.get(this.url + "clubform1?userId=" + userId + "&date=" + date);
  }
  clubform1Submit(id) {
    return this.http.get(this.url + "clubform1Submit?id=" + id);
  }
  clubform2(userId, date) {
    return this.http.get(this.url + "clubform2?userId=" + userId + "&date=" + date);
  }
  clubform2Submit(id) {
    return this.http.get(this.url + "clubform2Submit?id=" + id);
  }
  clubform3(userId, date) {
    return this.http.get(this.url + "clubform3?userId=" + userId + "&date=" + date);
  }
  clubform5(userId, date) {
    return this.http.get(this.url + "clubform5?userId=" + userId + "&date=" + date);
  }
  clubform3Submit(id) {
    return this.http.get(this.url + "clubform3Submit?id=" + id);
  }
  clubform4Submit(id) {
    return this.http.get(this.url + "clubform4Submit?id=" + id);
  }
  clubform5Submit(id) {
    return this.http.get(this.url + "clubform5Submit?id=" + id);
  }
  getLastStockUpdate() {
    return this.http.get(this.url + "getLastStockUpdate", this.httpOptions);
  }
  getUserDetail(id) {
    return this.http.get(this.url + "getUserDetail?id=" + id);
  }
  getClubMembershipList() {
    return this.http.get(this.url + "getClubMembershipList");
  }
  getFoldersName(id) {
    return this.http.get(this.url + "getFoldersName?id=" + id);
  }
  addFolderName(id, doctor_name) {
    return this.http.get(this.url + "addFolderName?id=" + id + "&doctor_name=" + doctor_name);
  }
  getVisualAids(id, user_id, folder_id) {
    return this.http.get(this.url + "getVisualAids?id=" + id + "&user_id=" + user_id + "&folder_id=" + folder_id);
  }
  addVisualAids(data) {
    return this.http.post(this.url + "addVisualAids", data, this.httpOptions);
  }
  getFolderVisuals(id) {
    return this.http.get(this.url + "getFolderVisuals?id=" + id);
  }
  deleteVisualAids(id, user_id) {
    return this.http.get(this.url + "deleteVisualAids?id=" + id + "&user_id=" + user_id);
  }
  getUserJFC(user_id) {
    return this.http.get(this.url + "getUserJFC?user_id=" + user_id, this.httpOptions);
  }
  getJFCMessages(jfc_id) {
    return this.http.get(this.url + "getJFCMessages?jfc_id=" + jfc_id, this.httpOptions);
  }
  getSingleJFC(jfc_id) {
    return this.http.get(this.url + "getSingleJFC?jfc_id=" + jfc_id, this.httpOptions);
  }
  booking(data) {
    return this.http.post(this.url + "booking", data, this.httpOptions2);
  }
  getBookingSubcategories(id) {
    return this.http.get(this.url + "getBookingSubcategories?id="+id, this.httpOptions);
  }
  getBookingCategories() {
    return this.http.get(this.url + "getBookingCategories", this.httpOptions);
  }
  getConfirmationBooking(id)
  {
    return this.http.get(this.url + "getConfirmationBooking?id=" + id, this.httpOptions);
  }
  getHYDPProducts() {
    return this.http.get(this.url + "getHYDPProducts", this.httpOptions);
  }
  getZermemoRates(id)
  {
    return this.http.get(this.url + "getZermemoRates?qty=" + id);
  }
  zermeroOrder(data)
  {
    return this.http.post(this.url +'zermeroOrder', data, this.httpOptions2);
  }
  helpyoudealbooking(data)
  {
    return this.http.post(this.url +'helpyoudealbooking', data, this.httpOptions2);
  }
  check_app_version()
  {
    return this.http.get(this.url + "check_app_version");    
  }
  getUserCats(id)
  {
    return this.http.get(this.url + "getUserCats?user_id=" + id);
  }
}
