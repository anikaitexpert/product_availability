import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.scss'],
})
export class RewardsComponent implements OnInit {
  shownGroup = null;
  rewards: any;
  user: any;
  user_id: any;
  loading = false;
  constructor(
    private navController: NavController) {

  
  }

  ngOnInit() {
   
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

}
