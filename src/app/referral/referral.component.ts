import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';


@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.scss'],
})
export class ReferralComponent implements OnInit {
  shownGroup = null;
  customers: any;
  user: any;
  user_id: any;
  loading = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.user_id = this.user.id;
    }
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  ngOnInit() {
    this.getCustomers();
  }

  async getCustomers() {
    this.loading = true;
    this.apiService.getCustomerList(this.user_id).subscribe((data: any) => {
      this.loading = false;
      this.customers = data.detail;
    });
  }

  getStatus(status)
  {
    if(status == 0)
    {
        return "On Hold";
    }
    else if( status == 1)
    {
      return "Converted";
    }else if(status == 2)
    {
      return "In Progress";
    }else if(status == 3)
    {
      return "About to Convert";
    }
  }
  getStatusColor(status)
  {
    if(status == 0)
    {
        return "danger";
    }
    else if( status == 1)
    {
      return "success";
    }else if(status == 2)
    {
      return "warning";
    }else if(status == 3)
    {
      return "primary";
    }
  }
  addReferral() {
    this.navController.navigateForward('/add-referral')
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  refresh() {
    this.getCustomers();
  }
}
