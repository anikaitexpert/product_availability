import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JfcComponent } from './jfc.component';

describe('JfcComponent', () => {
  let component: JfcComponent;
  let fixture: ComponentFixture<JfcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JfcComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
