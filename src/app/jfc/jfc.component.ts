import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-jfc',
  templateUrl: './jfc.component.html',
  styleUrls: ['./jfc.component.scss'],
})
export class JfcComponent implements OnInit {
  loading_message = "Loading...";
  loading = false;
  jfcsummaries:any;
  user: any;
  user_id: any;

  constructor(public localService: LocalService,
    public apiService: ApiService,
    public activatedRoute:ActivatedRoute,
    private navController: NavController,
    public commonService: CommonService) { 
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
      }
    }

  ngOnInit() {
    this.loading = true;
    this.apiService.getUserJFC(this.user_id).subscribe((data: any) => {    
      this.loading = false;  
      this.jfcsummaries = data.detail;

    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  jfcsummaryfun(id)
  {
    this.navController.navigateForward("/jfcsummary/"+id);
  }
}
