import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-clubdivision',
  templateUrl: './clubdivision.component.html',
  styleUrls: ['./clubdivision.component.scss'],
})
export class ClubdivisionComponent implements OnInit {
  loading_message = "Loading...";
  loading = false;
  categories:any;
  user: any;
  user_id: any;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
      }
  }

 async ngOnInit() {
    this.loading = true;
    this.apiService.getProducts(this.user_id).subscribe((data: any) => {    
      this.loading = false;  
      this.categories = data.detail;

    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  opencart()
  {
    this.navController.navigateForward('/cart');
  }

  productlist(id,catname)
  {
    this.navController.pop();
    this.navController.navigateForward('/clubform-list/'+id+"/"+catname);
  }
}
